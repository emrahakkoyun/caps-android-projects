package com.gojolo.caps;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
 static String iurl;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		final ImageButton img1 = (ImageButton)findViewById(R.id.imageButton1);
		img1.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                            img1.setBackgroundResource(R.drawable.cap2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img1.setBackgroundResource(R.drawable.cap1);
                         // Create intent to Open Image applications like Gallery, Google Photos
            				Intent galleryIntent = new Intent(Intent.ACTION_PICK,
            				        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            				// Start the Intent
            				startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                            break;}
                    }
            return false;
            }
        });
		
		
	}
	 @Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  // TODO Auto-generated method stub
	  super.onActivityResult(requestCode, resultCode, data);
      if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
              && null != data) {

       try {
           // When an Image is picked
           if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                   && null != data) {
               // Get the Image from data

               Uri selectedImage = data.getData();
               String[] filePathColumn = { MediaStore.Images.Media.DATA };

               // Get the cursor
               Cursor cursor = getContentResolver().query(selectedImage,
                       filePathColumn, null, null, null);
               // Move to first row
               cursor.moveToFirst();

               int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
               String imgDecodableString = cursor.getString(columnIndex);
               cursor.close();

               try {
      		     
     		      Bundle extras = new Bundle();
     		      extras.putString(iurl,imgDecodableString);
     		 
     		       Intent intent = new Intent();
     		       intent.putExtras(extras);
                     intent.setClass(getApplicationContext(),Image.class);
                    startActivity(intent);
     		    } catch (Exception e) {
     		      e.printStackTrace();
     		    }
           } else {
               Toast.makeText(this, "Resim Yüklenmedi",
                       Toast.LENGTH_LONG).show();
           }
       } catch (Exception e) {
           Toast.makeText(this, "yanlış birşeyler var", Toast.LENGTH_LONG)
                   .show();
       }
	  }
	  
	 }
}
