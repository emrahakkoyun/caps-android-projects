package com.gojolo.caps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
public class Image extends Activity {
 private Bundle extras=null;
 private String iurl,t;
 static String r1,r;
 static Bitmap gbitmap;
static ImageView img;
static String path;
static int nerde;
	EditText text;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);
	    //Burda AdView objesini oluşturuyoruz ve anasayfa.xml de oluşturduğumuz adView e bağlıyoruz
	    AdView adView = (AdView) this.findViewById(R.id.adView1);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		img = (ImageView)findViewById(R.id.img);
		Button btn1 = (Button)findViewById(R.id.btn1);
		Button btn2 = (Button)findViewById(R.id.btn2);
		Button btn3 = (Button)findViewById(R.id.btn3);
		final Button b1 = (Button)findViewById(R.id.button1);
		final Button b2 = (Button)findViewById(R.id.button2);
		final Button b3 = (Button)findViewById(R.id.button3);
		final Button b4 = (Button)findViewById(R.id.button4);
		final Button b5 = (Button)findViewById(R.id.button5);
		final Button b6 = (Button)findViewById(R.id.button6);
		final Button b7 = (Button)findViewById(R.id.button7);
		final Button b8 = (Button)findViewById(R.id.button8);
		final Button b9 = (Button)findViewById(R.id.button9);
		final Button b10 = (Button)findViewById(R.id.button10);
		final Button b11 = (Button)findViewById(R.id.button11);
		final Button b12 = (Button)findViewById(R.id.button12);
		final Button b13 = (Button)findViewById(R.id.button13);
		final Button b14 = (Button)findViewById(R.id.button14);
		final Button b15 = (Button)findViewById(R.id.button15);
		final Button b16 = (Button)findViewById(R.id.button16);
		final Button b17 = (Button)findViewById(R.id.button17);
		final Button b18 = (Button)findViewById(R.id.button18);
		final Button b19 = (Button)findViewById(R.id.button19);
		final Button b20 = (Button)findViewById(R.id.button20);
        text = (EditText)findViewById(R.id.text);
		InputFilter[] FilterArray = new InputFilter[1]; 
		FilterArray[0] = new InputFilter.LengthFilter(23); 
		text.setFilters(FilterArray);
		extras = getIntent().getExtras();
		iurl = extras.getString(MainActivity.iurl);
        img.setImageBitmap(BitmapFactory
                .decodeFile(iurl));
        btn1.setOnClickListener(new View.OnClickListener() {
        	
        	@Override
        	public void onClick(View v) {
        		if(text.toString()=="")
				{     Toast.makeText(getApplicationContext(), "lütfen yazı yazınız", Toast.LENGTH_SHORT).show();
				}
				else{
					Bitmap processedBitmap = ProcessingBitmap();
				     if(processedBitmap != null){
				      img.setImageBitmap(processedBitmap);
				      Toast.makeText(getApplicationContext(), 
				        "Done", 
				        Toast.LENGTH_LONG).show();
				     }else{
				      Toast.makeText(getApplicationContext(), 
				        "Something wrong in processing!", 
				        Toast.LENGTH_LONG).show();
				      nerde=0;
				     }
				    
				
			}}
        		
        	
        });
	btn2.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			savePicture(gbitmap,"caps"+".jpg");
			
		}
	});
	btn3.setOnClickListener(new  View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(nerde==1)
			{Intent sharingIntent = new Intent(Intent.ACTION_SEND);
			Uri screenshotUri = Uri.parse(path);
			sharingIntent.setType("image/png");
			sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			startActivity(Intent.createChooser(sharingIntent, "Share image using"));
			}else {Toast.makeText(getApplicationContext(), "Önce Caps Kaydet",Toast.LENGTH_LONG).show();}
			}
	});
	b1.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			t="#e10052";
			r=t;
			Bitmap processedBitmap = ProcessingBitmap();
		     if(processedBitmap != null){
		      img.setImageBitmap(processedBitmap);
		      b1.setText("✔");
		      b2.setText("");
		      b3.setText("");
		      b4.setText("");
		      b5.setText("");
		      b6.setText("");
		      b7.setText("");
		      b8.setText("");
		      b9.setText("");
		      b10.setText("");
			   nerde=0;
		     }
		}
	});
b2.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
		      b1.setText("");
		      b2.setText("✔");
		      b3.setText("");
		      b4.setText("");
		      b5.setText("");
		      b6.setText("");
		      b7.setText("");
		      b8.setText("");
		      b9.setText("");
		      b10.setText("");
		      nerde=0;
			t="#91007b";
			r=t;
			Bitmap processedBitmap = ProcessingBitmap();
		     if(processedBitmap != null){
		      img.setImageBitmap(processedBitmap);
		     }
		}
	});
b3.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("✔");
	      b4.setText("");
	      b5.setText("");
	      b6.setText("");
	      b7.setText("");
	      b8.setText("");
	      b9.setText("");
	      b10.setText("");
	      nerde=0;
		t="#3d107b";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b4.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("✔");
	      b5.setText("");
	      b6.setText("");
	      b7.setText("");
	      b8.setText("");
	      b9.setText("");
	      b10.setText("");
	      nerde=0;
		t="#314396";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b5.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("");
	      b5.setText("✔");
	      b6.setText("");
	      b7.setText("");
	      b8.setText("");
	      b9.setText("");
	      b10.setText("");
	      nerde=0;
		t="#00a8ec";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b6.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("");
	      b5.setText("");
	      b6.setText("✔");
	      b7.setText("");
	      b8.setText("");
	      b9.setText("");
	      b10.setText("");
	      nerde=0;
		t="#009f3c";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b7.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("");
	      b5.setText("");
	      b6.setText("");
	      b7.setText("✔");
	      b8.setText("");
	      b9.setText("");
	      b10.setText("");
	      nerde=0;
		t="#7cc623";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b8.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("");
	      b5.setText("");
	      b6.setText("");
	      b7.setText("");
	      b8.setText("✔");
	      b9.setText("");
	      b10.setText("");
	      nerde=0;
		t="#f8f400";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b9.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("");
	      b5.setText("");
	      b6.setText("");
	      b7.setText("");
	      b8.setText("");
	      b9.setText("✔");
	      b10.setText("");
	      nerde=0;
		t="#ef9c00";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b10.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b1.setText("");
	      b2.setText("");
	      b3.setText("");
	      b4.setText("");
	      b5.setText("");
	      b6.setText("");
	      b7.setText("");
	      b8.setText("");
	      b9.setText("");
	      b10.setText("✔");
	      nerde=0;
		t="#ffffff";
		r=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b11.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b11.setText("✔");
	      b12.setText("");
	      b13.setText("");
	      b14.setText("");
	      b15.setText("");
	      b16.setText("");
	      b17.setText("");
	      b18.setText("");
	      b19.setText("");
	      b20.setText("");
	      nerde=0;
		t="#e10052";
		r1=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b12.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
	      b11.setText("");
	      b12.setText("✔");
	      b13.setText("");
	      b14.setText("");
	      b15.setText("");
	      b16.setText("");
	      b17.setText("");
	      b18.setText("");
	      b19.setText("");
	      b20.setText("");
	      nerde=0;
		t="#91007b";
		r1=t;
		Bitmap processedBitmap = ProcessingBitmap();
	     if(processedBitmap != null){
	      img.setImageBitmap(processedBitmap);
	     }
	}
});
b13.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("✔");
    b14.setText("");
    b15.setText("");
    b16.setText("");
    b17.setText("");
    b18.setText("");
    b19.setText("");
    b20.setText("");
    nerde=0;
	t="#3d107b";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b14.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("✔");
    b15.setText("");
    b16.setText("");
    b17.setText("");
    b18.setText("");
    b19.setText("");
    b20.setText("");
    nerde=0;
	t="#314396";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b15.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("");
    b15.setText("✔");
    b16.setText("");
    b17.setText("");
    b18.setText("");
    b19.setText("");
    b20.setText("");
    nerde=0;
	t="#00a8ec";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b16.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("");
    b15.setText("");
    b16.setText("✔");
    b17.setText("");
    b18.setText("");
    b19.setText("");
    b20.setText("");
    nerde=0;
	t="#009f3c";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b17.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("");
    b15.setText("");
    b16.setText("");
    b17.setText("✔");
    b18.setText("");
    b19.setText("");
    b20.setText("");
    nerde=0;
	t="#7cc623";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b18.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("");
    b15.setText("");
    b16.setText("");
    b17.setText("");
    b18.setText("✔");
    b19.setText("");
    b20.setText("");
    nerde=0;
	t="#f8f400";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b19.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("");
    b15.setText("");
    b16.setText("");
    b17.setText("");
    b18.setText("");
    b19.setText("✔");
    b20.setText("");
    nerde=0;
	t="#ef9c00";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
b20.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {
    b11.setText("");
    b12.setText("");
    b13.setText("");
    b14.setText("");
    b15.setText("");
    b16.setText("");
    b17.setText("");
    b18.setText("");
    b19.setText("");
    b20.setText("✔");
    nerde=0;
	t="#ffffff";
	r1=t;
	Bitmap processedBitmap = ProcessingBitmap();
     if(processedBitmap != null){
      img.setImageBitmap(processedBitmap);
     }
}
});
	}
	private Bitmap ProcessingBitmap(){
		  Bitmap newBitmap = null;
			Bitmap bmp1 = BitmapFactory
			        .decodeFile(iurl);
		  try {
		   
		   Config config = bmp1.getConfig();
		   if(config == null){
		    config = Bitmap.Config.ARGB_8888;
		   }
		   
		   newBitmap = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), config);
		   Canvas newCanvas = new Canvas(newBitmap);
		   
		   newCanvas.drawBitmap(bmp1, 0, 0, null);
		   
		   String captionString = text.getText().toString();
		   if(captionString != null){
			   Paint paint = new Paint();
			   paint.setStyle(Paint.Style.FILL);
			   paint.setAntiAlias(true);
			   float h=newCanvas.getHeight();
			   float w=newCanvas.getWidth();
			   paint.setColor(Color.parseColor(r1));
			   newCanvas.drawRect(0,h-70, w+70,h, paint);
					paint.setColor(Color.parseColor(r));
			   paint.setTextAlign(Align.CENTER);
			   paint.setTextSize(45);
			   newCanvas.drawText(captionString, w/2, h-20, paint);
			   gbitmap=newBitmap;
		    Toast.makeText(getApplicationContext(), 
		      "Oluşturuldu", 
		      Toast.LENGTH_LONG).show();
		    
		   }else{
		    Toast.makeText(getApplicationContext(), 
		      "oluşturulmadı!", 
		      Toast.LENGTH_LONG).show();
		   }
		   
		  } catch (Exception e1) {
		   // TODO Auto-generated catch block
		   e1.printStackTrace();
		  }
		  
		  return newBitmap;
		 }
	private void savePicture(Bitmap bm, String imgName)
	 {  
	  OutputStream fOut = null;
	  String strDirectory = Environment.getExternalStorageDirectory().toString();

	  File f = new File(strDirectory, imgName);
	  try {
	   fOut = new FileOutputStream(f);
	   
	   /**Compress image**/
	   bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
	   fOut.flush();
	   fOut.close();

	   /**Update image to gallery**/
	   path = MediaStore.Images.Media.insertImage(getContentResolver(),
	    f.getAbsolutePath(), f.getName(), f.getName());
	   nerde=1;
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
	 }

}
